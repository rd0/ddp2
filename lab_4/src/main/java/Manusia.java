public class Manusia{
	private String nama, data;
	private int umur, uang;
	private float kebahagiaan;

	public Manusia(String nama, int umur){
		this.nama = nama;
		this.umur = umur;
		this.uang = 50000;
		this.kebahagiaan = 50;
		this.data = "";
	}

	public Manusia(String nama, int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
		this.data = "";
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return this.nama;
	}

	public void setUmur(int umur){
		this.umur = umur;
	}

	public int getUmur(){
		return this.umur;
	}

	public void setUang(int uang){
		this.uang = uang;
	}

	public int getUang(){
		return this.uang;
	}

	public void setKebahagiaan(float kebahagiaan){
		if (kebahagiaan > 100)
			this.kebahagiaan = 100;
		else if(kebahagiaan < 0)
			this.kebahagiaan = 0;
		else
			this.kebahagiaan = kebahagiaan;
	}

	public float getKebahagiaan(){
		return this.kebahagiaan;
	}

	public void beriUang(Manusia penerima){
		int jumlah = 0;
		for(int i = 0; i < penerima.getNama().length(); i++)
			jumlah += penerima.getNama().charAt(i);
		jumlah *= 100;
		if(this.getUang() >= jumlah){
			// tambah kebahagiaan
			this.setKebahagiaan(this.getKebahagiaan() + (float)jumlah/6000); penerima.setKebahagiaan(penerima.getKebahagiaan() + (float)jumlah/6000);
			// tambah uang penerima dan kurang uang pemberi
			this.setUang(this.getUang() - jumlah); penerima.setUang(penerima.getUang() + jumlah);

			System.out.println(this.getNama() + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
		}
		else // uang pemberi tidak cukup
			System.out.println(this.getNama() + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak cukup uang :'(");
	}

	public void beriUang(Manusia penerima, int jumlah){
		if(this.getUang() >= jumlah){
			// tambah kebahagiaan
			this.setKebahagiaan(this.getKebahagiaan() + (float)jumlah/6000); penerima.setKebahagiaan(penerima.getKebahagiaan() + (float)jumlah/6000);
			// tambah uang penerima dan kurang uang pemberi
			this.setUang(this.getUang() - jumlah); penerima.setUang(penerima.getUang() + jumlah);

			System.out.println(this.getNama() + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
		}
		else
			System.out.println(this.getNama() + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak cukup uang :'(");
	}

	public void bekerja(int durasi, int bebanKerja){
		if(this.getUmur() < 18)
			System.out.println(this.getNama() + "belum boleh bekerja karena masih dibawah umur D:");
		else{
			int bebanKerjaTotal = durasi * bebanKerja, pendapatan;
			if((float)bebanKerjaTotal <= this.getKebahagiaan()){
				pendapatan = bebanKerjaTotal * 10000;
				// kebahagiaan berkurang
				this.setKebahagiaan(this.getKebahagiaan() - bebanKerjaTotal);

				System.out.println(this.getNama() + " bekerja full time, total pendapatan : " + pendapatan);
			}
			else{
				int durasiBaru = (int)this.getKebahagiaan() / bebanKerja;
				bebanKerjaTotal = durasiBaru * bebanKerja;
				pendapatan =bebanKerjaTotal * 10000;
				// kebahagiaan berkurang
				this.setKebahagiaan(this.getKebahagiaan() - bebanKerjaTotal);

				System.out.println(this.getNama() + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);				
			}
			this.setUang(this.getUang() + pendapatan);
		}
	}

	public void rekreasi(String namaTempat){
		int biaya = namaTempat.length() * 10000;
		if(this.getUang() >= biaya){
			// uang berkurang
			this.setUang(this.getUang() - biaya);
			// kebahagiaan bertambah
			this.setKebahagiaan(this.getKebahagiaan() + namaTempat.length());

			System.out.println(this.getNama() + " berekreasi di " + namaTempat + ", " + this.getNama() + " senang :)");
		}
		else
			System.out.println(this.getNama() + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat);
	}

	public void sakit(String namaPenyakit){
		// kebahagiaan berkurang
		this.setKebahagiaan(this.getKebahagiaan() - namaPenyakit.length());

		System.out.println(this.getNama() + " terkena penyakit " + namaPenyakit + " :O");
	}

	public void meninggal(){
		System.out.println(this.getNama() + " meninggal dengan tenang, kebahagiaan : " + this.getKebahagiaan());

	}

	public String toString(){
		data = "";
		data += "Nama\t\t: " + this.getNama() + "\n";
		data += "Umur\t\t: " + this.getUmur() + "\n";
		data += "Uang\t\t: " + this.getUang() + "\n";
		data += "kebahagiaan\t: " + this.getKebahagiaan();
		return data;
	}
}