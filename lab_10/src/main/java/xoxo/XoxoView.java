package xoxo;

import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Yusuf T Ardho
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        // input panel
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(4, 1));

            // message input
            JPanel messagePanel = new JPanel(new GridLayout(1,2));
            JLabel messageLabel = new JLabel("   Message: ");
            messageField = new JTextField(1000);
            messagePanel.add(messageLabel);
            messagePanel.add(messageField);
            inputPanel.add(messagePanel);

            // key input
            JPanel keyPanel = new JPanel(new GridLayout(1,2));
            JLabel keyLabel = new JLabel("   Key: ");
            keyField = new JTextField(1000);
            keyPanel.add(keyLabel);
            keyPanel.add(keyField);
            inputPanel.add(keyPanel);

            // seed input
            JPanel seedPanel = new JPanel(new GridLayout(1,2));
            JLabel seedLabel = new JLabel("   Seed: ");
            seedField = new JTextField(1000);
            seedPanel.add(seedLabel);
            seedPanel.add(seedField);
            inputPanel.add(seedPanel);

        // encrypt & decrypt button panel
        JPanel buttonPanel = new JPanel(new GridLayout(1, 2));
        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton("Decrypt");
        buttonPanel.add(encryptButton);
        buttonPanel.add(decryptButton);

        // main panel
        JPanel mainPanel = new JPanel(new BorderLayout());
        inputPanel.add(buttonPanel);
        mainPanel.add(inputPanel, BorderLayout.NORTH);
        logField = new JTextArea("Log Activity:\n");
        mainPanel.add(logField, BorderLayout.CENTER);

        // main frame
        JFrame mainFrame = new JFrame("Lab 10");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.add(mainPanel);
        mainFrame.setSize(800, 600);
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    /**
     * Show the alert dialog when input trigger the exception.
     * @param alertMessage A message that inform the exception occur.
     */
    public void warningWindow(String alertMessage) {
        JOptionPane.showMessageDialog(logField, alertMessage,
         "Error!", JOptionPane.WARNING_MESSAGE);
    }

}