package xoxo.exceptions;

/**
 * An exception that is thrown if the seed 
 * greater than 36 or lower than 0.
 *
 * @author Yusuf T Ardho
 */
public class RangeExceededException extends RuntimeException {

    /**
     * Class constructor.
     */
    public RangeExceededException(String message) {
        super(message);
    }

}