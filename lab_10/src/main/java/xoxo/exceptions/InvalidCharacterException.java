package xoxo.exceptions;

/**
 * An exception that is'nt thrown if a string contains
 * only 'a'-'z' or 'A'-'Z' or '@' character.
 *
 * @author Yusuf T Ardho
 */
public class InvalidCharacterException extends RuntimeException {
	
	/**
	 * Class constructor.
	 */
    public InvalidCharacterException(String message) {
    	super(message);
    }

}