package xoxo.exceptions;

/**
 * An exception that is thrown if the size of
 * the message is more than 10Kb.
 *
 * @author Yusuf T Ardho
 */
public class SizeTooBigException extends RuntimeException {

    /**
     * Class constructor.
     */
    public SizeTooBigException(String message) {
        super(message);
    }
    
}