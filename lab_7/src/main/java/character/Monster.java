package character;

//  write Monster Class here
public class Monster extends Player{

	public Monster(String name, int hp, String roar){
		super(name, 2*hp, "Monster", roar);
	}

	public Monster(String name, int hp){
		super(name, 2*hp, "Monster", "AAAAAAaaaAAAAAaaaAAAAAA");
	}
}