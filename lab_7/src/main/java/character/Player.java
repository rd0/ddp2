package character;
import java.util.ArrayList;

//  write Player Class here
public class Player{

	protected ArrayList<Player> eaten;
	protected String name, type, roar;
	protected boolean isBurned;
	protected int hp;

	public Player(String name, int hp, String type){
		this.name = name;
		this.hp = hp;
		this.type = type;
		this.isBurned = false;
		this.eaten = new ArrayList<Player>();
	}

	public Player(String name, int hp, String type, String roar){
		this.name = name;
		this.hp = hp;
		this.type = type;
		this.isBurned = false;
		this.roar = roar;
		this.eaten = new ArrayList<Player>();
	}

	/* Setter & Getter */
	public ArrayList<Player> getEaten(){
		return this.eaten;
	}

	public String getName(){
		return this.name;
	}

	public boolean getIsBurned(){
		return this.isBurned;
	}

	public void setIsBurned(boolean status){
		this.isBurned = status;
	}

	public String getType(){
		return this.type;
	}

	public int getHp(){
		return this.hp;
	}

	public void setHP(int hp){
		if (hp <= 0)
			this.hp = 0;
		else
			this.hp = hp;
	}

	public boolean canEat(Player enemy){
		if ((!this.getType().equals("Monster") && 
			!enemy.getType().equals("Monster")) ||
			enemy.getHp() != 0)
            	return false;
        return true;
	}

	/* command */
	public void attack(Player enemy){
		if (enemy.getType().equals("Magician"))
			enemy.setHP(enemy.getHp() - 20);
		else
			enemy.setHP(enemy.getHp() - 10);
	}

	public void eat(Player enemy){
		setHP(this.getHp() + 15);
		this.eaten.add(enemy);
	}

	public void burn(Player enemy){
		if (enemy.getType().equals("Magician"))
			enemy.setHP(enemy.getHp() - 20);
		else
			enemy.setHP(enemy.getHp() - 10);

		if (enemy.getHp() == 0)
			enemy.setIsBurned(true);
	}

	public String roar(){
		return this.roar;
	}

	/* "<Type> <Name>" */
	public ArrayList<String> diet(){
		ArrayList<String> dietList = new ArrayList<String>();
		for (int i = 0; i < this.getEaten().size(); i++)
			dietList.add(this.getEaten().get(i).getType()+" "+this.getEaten().get(i).getName());
		return dietList;
	}

	/* 
		Status format:

		Line-1: "<Type> <Name>"
        Line-2: "HP: <HP>"
        Line-3: "Sudah meninggal dunia dengan damai" / "Masih hidup"
        Line-4: "Memakan <Type> <Name>"
        		"Memakan <Type> <Name>"
        		........................ /
        		"Belum memakan siapa siapa"
	*/
	public String status(){
		String player_stat = "";

		player_stat += String.format("%s %s\nHP: %d\n", 
						this.getType(), this.getName(), this.getHp());

		if (this.getHp() != 0)
			player_stat += "Masih Hidup\n";
		else
			player_stat += "Sudah meninggal dunia dengan damai\n";

		if (this.eaten.size() == 0)
			player_stat += "Belum memakan siapa siapa";
		else
			for (int i = 0; i < this.diet().size(); i++){
				player_stat += "Memakan " + this.diet().get(i);

				if (i != this.diet().size() - 1)
					player_stat += "\n";
			}
		
		return player_stat;
	}
}
