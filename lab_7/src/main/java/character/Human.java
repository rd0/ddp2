package character;

//  write Human Class here
public class Human extends Player{

	public Human(String name, int hp){
		super(name, hp, "Human");
	}

	public Human(String name, int hp, String type){
		super(name, hp, type);
	}

}