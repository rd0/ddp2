import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (int i = 0; i < player.size(); i++)
            if (name.equals(player.get(i).getName()))
                return player.get(i);

        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        /* check */
        if (find(chara) != null)
            return "Sudah ada karakter bernama " + chara;

        /* valid case */
        if (tipe.equals("Human"))
            player.add(new Human(chara, hp));
        else if (tipe.equals("Monster"))
            player.add(new Monster(chara, hp));
        else // (tipe.equals("Magician"))
            player.add(new Magician(chara, hp));

        return chara + " ditambah ke game";
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        /* check */
        if (find(chara) != null)
            return "Sudah ada karakter bernama " + chara;

        /* valid case */
        player.add(new Monster(chara, hp, roar));
        return chara + "ditambah ke game";
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        Player objChara = find(chara);

        /* check */
        if (objChara == null)
            return "Tidak ada " + chara;

        /* valid case */
        player.remove(objChara);
        return chara + " dihapus dari game";
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        Player objChara = find(chara);

        /* check */
        if (objChara == null)
            return "Tidak ada pemain";

        /* valid case */
        return objChara.status();
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        /* check */
        if (player.size() == 0)
            return "Tidak ada pemain";

        /* valid case */
        String all_player = "";
        for (int i = 0; i < player.size(); i++){
            all_player += player.get(i).status();

            if (i != player.size() - 1)
                all_player += "\n";
        }
        return all_player;        
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        Player objChara = find(chara);

        /* check */
        if (objChara == null || objChara.diet().size() == 0)
            return "Belum ada yang termakan";

        /* valid case */
        String diet_list = ""; // format: aa bb, cc dd, ..., yy zz 
        for (int i = 0; i < objChara.diet().size(); i++){
            diet_list += objChara.diet().get(i);
            if (i != objChara.diet().size() - 1)
                diet_list += ", ";
        }

        return diet_list;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        /* check */
        for (int i = 0; i < player.size(); i++){
            if (player.get(i).getEaten().size() != 0) // at least 1 = valid case
                break;

            if (i == player.size() - 1)  // all empty
                return "Belum ada yang termakan";
        }

        /* valid case */
        ArrayList<String> all_diet_list = new ArrayList<String>();
        for (int i = 0; i < player.size(); i++){
            Player current_obj = player.get(i);

            for (int j = 0; j < current_obj.diet().size(); j++)
                all_diet_list.add(current_obj.diet().get(j));
        }

        String inString = "";
        for (int i = 0; i < all_diet_list.size(); i++){
            inString += all_diet_list.get(i);

            if (i != all_diet_list.size() - 1)
               inString += ", ";
        }

        return "Termakan : " + inString;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player objMe = find(meName), objEne = find(enemyName);

        /* check */
        if (objMe == null || objEne == null)
            return "Tidak ada pemain";

        /* valid case */
        objMe.attack(objEne);
        return String.format("Nyawa %s %d", enemyName, objEne.getHp());
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        /* check */
        if (find(meName) == null || find(enemyName) == null)
            return "Tidak ada pemain";
        if (!find(meName).getType().equals("Magician")) 
            return meName + " bukan magician";

        /* valid case */
        find(meName).burn(find(enemyName));
        if (find(enemyName).getIsBurned())  
            return "Nyawa " + enemyName + " 0 \n dan matang";
        else  
            return String.format("Nyawa %s %d", enemyName, find(enemyName).getHp());
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player objMe = find(meName), objEne = find(enemyName);

        /* check */
        if (objMe == null || objEne == null)
            return "Tidak ada pemain";

        if (!objMe.canEat(objEne))
                return meName + " tidak bisa memakan " + enemyName;

        /* valid case */
        objMe.eat(objEne);
        player.remove(objEne);
        return String.format("%s memakan %s\nNyawa %s kini %d",
                            meName, enemyName, meName, objMe.getHp());
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player objMe = find(meName);

        /* check */
        if (objMe == null)
            return "Tidak ada " + meName;
        if (!objMe.getType().equals("Monster"))
            return meName + " tidak bisa berteriak";

        /* valid case */
        return objMe.roar();
    }
}