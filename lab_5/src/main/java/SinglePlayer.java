import java.util.Scanner;

public class SinglePlayer{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);

		Number[][] data = new Number[5][5]; 
		Number[] ds = new Number[100]; // data states
		int temp; String command;
		
		for(int i = 0; i < 5; i++){
			for(int j = 0; j < 5; j++){
				temp = input.nextInt();
				data[i][j] = new Number(temp, i, j);
				ds[temp] = data[i][j];
			}
		}

		BingoCard card = new BingoCard(data, ds);
		
		String[] inp;

		while(!card.isBingo()){
			inp = input.nextLine().split(" ");
			switch(inp[0]){
				case "MARK":
					temp = Integer.parseInt(inp[1]);
					System.out.println(card.markNum(temp));
					break;
				case "INFO":
					System.out.println(card.info());
					break;
				case "RESTART":
					card.restart();
					break;
			}
		}
		System.out.print("BINGO!\n");

		
	}	
}