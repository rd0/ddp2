package customer;
import movie.Movie;
import ticket.Ticket;
import theater.Theater;
import java.util.ArrayList;

public class Customer{
	private String nama;
	private int umur;
	private String jenis_kelamin;
	private ArrayList<Ticket> listTiket, watched;

	// nama, jenis kelamin, umur
	public Customer(String nama, String jenis_kelamin, int umur){
		this.nama = nama;
		this.jenis_kelamin = jenis_kelamin;
		this.umur = umur;
		this.listTiket = new ArrayList<Ticket>(); // tiket yang dimiliki customer
		this.watched = new ArrayList<Ticket>(); // film yang sudah ditonton customer
	}

	public String getNama(){
		return this.nama;
	}

	public int getUmur(){
		return this.umur;
	}

	public boolean adaFilmnya(Ticket tiket, String judul, String hari, String jenis){
		// Judul film ada, Jadwal film ada, Jenisnya sesuai
		if (tiket.getFilm().getJudul().equals(judul) && tiket.getJadwal().equals(hari) &&
			(jenis.equals("Biasa") && !tiket.getJenis() || jenis.equals("3 Dimensi") && tiket.getJenis()))
				return true;
		return false;
	}

	public boolean cukupUmur(Ticket tiket, int umur){
		// Dewasa >= 17, Remaja >= 13
		return tiket.getFilm().getRating().equals("Dewasa") && umur >= 17 ||
			tiket.getFilm().getRating().equals("Remaja") && umur >= 13 ||
			tiket.getFilm().getRating().equals("Umum");
	}

	public Ticket orderTicket(Theater bioskop, String judul, String jadwal, String jenis){
		int nTiket = bioskop.getNTiket();
		boolean filmAda = false;
		String rating = "";

		// iterasiin semua tiket di bioskop
		for (int idx = 0; idx < nTiket; idx++){
			// tiket (di bioskop) ke-idx
			Ticket tiket_i = bioskop.getListTiket().get(idx);

			if (adaFilmnya(tiket_i, judul, jadwal, jenis)){
				filmAda = true;
				rating = bioskop.getListTiket().get(idx).getFilm().getRating();
			}

			if (adaFilmnya(tiket_i, judul, jadwal, jenis) && cukupUmur(tiket_i, umur)){
				System.out.printf("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d\n",
					this.getNama(), tiket_i.getFilm().getJudul(), jenis, bioskop.getNama(), jadwal, tiket_i.getHarga());
				bioskop.setSaldo(bioskop.getSaldo() + tiket_i.getHarga());
				this.listTiket.add(tiket_i);
				return tiket_i;
			}
		}

		if (filmAda)
			System.out.printf("%s masih belum cukup umur untuk menonton %s dengan rating %s\n",
				this.getNama(), judul, rating);
		else
			System.out.printf("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s\n",
				judul, jenis, jadwal, bioskop.getNama());

		return null;
	}

	// bonus, this = customernya
	public void watchMovie(Ticket tiket){
		// tambahkan tiket ke watched list
		this.watched.add(tiket);
		System.out.printf("%s telah menonton film %s\n",
			this.getNama(), tiket.getFilm().getJudul());
	}

	public boolean isWatched(Ticket tiket){
		// iterasiin semua tiket yang udah ditonton this
		for (int i = 0; i < this.watched.size(); i++)
			if (this.watched.get(i).equals(tiket))
				return true;
		return false;
	}

	public void cancelTicket(Theater bioskop){
		// tiket = tiket yang paling baru di beli
		Ticket tiket = this.listTiket.get(this.listTiket.size()-1);

		// iterasiin semua tiket di bioskop
		for (int i = 0; i < bioskop.getNTiket(); i++){
			Ticket currentTicket_i = bioskop.getListTiket().get(i);
			
			// cek apakah tiketnya sama
			if (currentTicket_i.getFilm().equals(tiket.getFilm())){
				// cek apakah cukup untuk refund dan belum di tonton
				if (bioskop.getSaldo() >= tiket.getHarga() && !isWatched(tiket)){
					// kembaliin tiketnya dulu
					bioskop.setListTiket(tiket);
					// kurangin saldo bioskop
					bioskop.setSaldo(bioskop.getSaldo() - tiket.getHarga());
					// tiket (ke-i) this dikembalikan
					this.listTiket.remove(this.listTiket.size()-1);
					System.out.printf("Tiket film %s dengan waktu tayang %s jenis %s dikembalikan ke bioskop %s\n",
							tiket.getFilm().getJudul(), tiket.getJadwal(),
							tiket.getJenis() ? "3 Dimensi" : "Biasa", bioskop.getNama());
					return;
				}
				else if (isWatched(tiket)){
					System.out.printf("Tiket tidak bisa dikembalikan karena film %s sudah ditonton oleh %s\n",
						tiket.getFilm().getJudul(), this.nama);
					return;
				}
				else { // saldo bioskop nggak cukup
					System.out.printf("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop %s lagi tekor...\n",
						bioskop.getNama());
					return;
				}
			}
		}
		System.out.printf("Maaf tiket tidak bisa dikembalikan, %s tidak tersedia dalam %s\n",
			tiket.getFilm().getJudul(), bioskop.getNama());
	}

	public void findMovie(Theater bioskop, String judul){
		boolean filmAda = false;
		for (int idx = 0; idx < bioskop.getNFilm(); idx++){
			// film ke-idx di bioskop
			Movie film_i = bioskop.getListFilm()[idx];

			if (film_i.getJudul().equals(judul)){
				filmAda = true;
				System.out.println("------------------------------------------------------------------");
				System.out.printf("Judul   : %s\n", judul);
				System.out.printf("Genre   : %s\n", film_i.getGenre());
				System.out.printf("Durasi  : %d menit\n", film_i.getDurasi());
				System.out.printf("Rating  : %s\n", film_i.getRating());
				System.out.printf("Jenis   : Film %s\n", film_i.getJenis());
				System.out.println("------------------------------------------------------------------");
			}
		}
		if (!filmAda)
			System.out.printf("Film %s yang dicari %s tidak ada di bioskop %s\n",
				judul, nama, bioskop.getNama()); 
	} 
}
