package movie;

public class Movie{
	private String judul, genre, jenis, rating;
	private int durasi;

	// judul, rating(dewasa/remaja/umum), durasi, genre, jenis(lokal/impor)
	public Movie(String judul, String rating, int durasi, String genre, String jenis){
		this.judul = judul;
		this.genre = genre;
		this.durasi = durasi;
		this.rating = rating;
		this.jenis = jenis;
	}

	public String getJudul(){
		return this.judul;
	}

	public String getRating(){
		return this.rating;
	}

	public String getGenre(){
		return this.genre;
	}

	public int getDurasi(){
		return this.durasi;
	}

	public String getJenis(){
		return this.jenis;
	}
}